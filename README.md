# HttpPythonLog3

**Introduction** 

HttpPythonLog3 is the Python 3.6 version of the logstuff api driver.

**HttpPythonLog** is a python driver to assist in the interaction with a IOT/telemetry type online logging api called **LogMiniAPI**. This enable you to make your own python project become a IOT/telemetry reporting tool, whether the IOT data you gather is to debug your application or wheteher it is to log some hardware, it does not really matter. It is just a simple online logging application.
When **HttpPythonLog** interacts with **LogMiniAPI** the data sent to **LogMiniAPI** gets stored in a online database.
To View the stored telemetry data that you have logged, you can go to the MVC application online and select your **datasets**, the **datasets** will then be plotted for you by the MVC applcation. There is a online version of he MVC application running at [logyhog]https://www.logyhog.com/

(A driver for the API can be in any lauguage as long as it can make http - https calls and receive/send json data.)

---

#Usage of HttpPythonLog

1. Clone/download the sourc **Source**.
2. pip install it or import it to your Python source.

Some examples will follow below.

---

## Example

To install: Navigate to the folder containing the setup.py file then run...

```sh
C:\Workspace\HttpPythonLog>pip install -e .
```
To uninstall (from any where run):

```sh
C:\>pip uninstall HttpPythonLog
Uninstalling HttpPythonLog-0.1:
  Would remove:
    c:\python27\lib\site-packages\httppythonlog.egg-link
Proceed (y/n)? y
  Successfully uninstalled HttpPythonLog-0.1
```

---

## How it works


A Task can hold multiple datasets.
A Dataset can hold multiple types of telemetry.
Rectangles, Text, XY and Y vs datetime data
Put your ip address and the port number in the function below.
When you create a task in logstuff, give it a name, a 6 digit code will be created for you
```python
logstuff = IP_token_Task_User("http://10.18.24.100:9673", "6_digit_task_code", "Task_name")
```
Type any name, it will be added to the Task in logstuff as you log it.
Set limits for you dataset, if you want for you xy data.
setlimits("datset_name", x_high_limit, y_high_limit, x_low_limit, y_low_limit)

```python
v = logstuff.setlimits_dataset("dataset_name1000", 50, 0, 10, 10)
```
log some xy values to it and check on logstuff if the pass or fail
```python
logstuff.xy("dataset_name1000", 40, 5)
```
To log text to the same dataset
```python
logstuff.text("dataaset_name1000", "Text in my dataset")
```
To log text to a new, different dataset, the dataset will be created if it does not exist
```python
logstuff.text("dataaset_name1001", "Text in my dataset")
```
To log y vs datatime values.
```python
logstuff.y("dataset_name1001", 7)
```
---