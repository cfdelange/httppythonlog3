import unittest
from httppythonlog.Helper import requesthelper_queue
import time

class test_helper(unittest.TestCase):
    def setUp(self):
        print("Main thread!!: setup")

    def test_requesthelper_queue(self):
        print("Main thread!!: start")
        for i in range(5):
            requesthelper_queue("{}: sd/sd/sd/sd".format(i))
        print("Main thread!!: Done load first 10")
        time.sleep(12)
        for i in range(11, 13):
            requesthelper_queue("{}: sd/sd/sd/sd".format(i))
        print("Main thread!!: Done load SECOND 10")
        time.sleep(15)
        print("Main thread!!: sleepDone")


    def test_stop_and_start_worker(self):
        print("Main thread!!: start")
        for i in range(2):
            requesthelper_queue("{}: sd/sd/sd/sd".format(i))
        print("Main thread!!: Done load first 10")
        time.sleep(15)
        for i in range(11, 14):
            requesthelper_queue("{}: sd/sd/sd/sd".format(i))
        print("Main thread!!: Done load SECOND 10")
        time.sleep(20)
        print("Main thread!!: sleepDone")
