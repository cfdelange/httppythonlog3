
driverInJson = {
    'bcolorset': False,
    'Color': {
        'A': 0,
        'R': 0,
        'G': 0,
        'B': 0
    },
    'UpdateReqdhour': 7
}

class MyColor:
    def __init__(self):
        self.A = 0
        self.R = 0
        self.G = 0
        self.B = 0

class DriverInput(object):

    def __init__(self, DriverInput):
        self.datasetID = DriverInput[u'datasetID']
        self.ID = DriverInput[u'ID']
        self.jsondata = DriverInput[u'jsondata']