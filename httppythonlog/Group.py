"""
Groups hold GroupItems. GroupItems are DataSets the should be display
together in a group.
"""

class Group(object):

    def __init__(self, group):
        self.groupID = group[u'groupID']
        self.groupName = group[u'groupName']
        self.count = group[u'count']
        self.limit = group[u'limit']
        self.dateCreated = group[u'dateCreated']
        self.taskID = group[u'taskID']
