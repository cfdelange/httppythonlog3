from urllib.parse import urljoin
from httppythonlog.Helper import requesthelper, requesthelper_queue, requesthelperWithHeader
from httppythonlog.StatusVscolor import StatusVsColor


class LogStuff_Logger(object):

    def __init__(self, host, dataset):
        self._host = host
        self._token = dataset.sixDigitCode
        self._datasetName = dataset.name
        self._datasetid = dataset.datasetID
        self.dataset = dataset
        self.metaid = dataset.meta.id

    def LogRectangle(self, x, y, w, h, burst=False):
        _apiqueryurl = urljoin(
            self._host,
            "api/Rect/Create/{4}/{5}/{0}/{1}/{2}/{3}/{6}".format
            (
                x, y, w, h, self._token, self._datasetName, self._datasetid
            ))
        return self._send_itWithHeader(_apiqueryurl,  self._token, self._datasetid, self._datasetName)

    def LogY(self, y, burst=False):
        _apiqueryurl = urljoin(
            self._host,
            "api/Y/Create/{1}/{2}/{0}/{3}".format
            (
                y, self._token, self._datasetName, self._datasetid
            ))
        return self._send_itWithHeader(_apiqueryurl, self._token, self._datasetid, self._datasetName)

    def LogText(self, text, y=0, burst=False):
        _apiqueryurl = urljoin(
            self._host,
            "api/Text/Create/{0}/{1}/{2}/{3}/{4}".format
            (
                self._token, self._datasetName, text, y, self._datasetid
            ))
        return self._send_itWithHeader(_apiqueryurl, self._token, self._datasetid, self._datasetName)

    def LogXY(self, x, y, burst=False):
        _apiqueryurl = urljoin(
            self._host,
            "api/Coord/Create/{2}/{3}/{0}/{1}/{4}".format
            (
                x, y, self._token, self._datasetName, self._datasetid
            ))
        return self._send_itWithHeader(_apiqueryurl, self._token, self._datasetid, self._datasetName)

    def DatasetPass(self, b_truefalse, burst=False):
        if not isinstance(b_truefalse, bool):
            return {"httppythonlog3" : "not_boolean"}
        _apiqueryurl = urljoin(
            self._host,
            "api/Meta/UpdatePassFail/{1}/{2}/{0}/{3}".format
            (
                b_truefalse, self._token, self._datasetName, self._datasetid
            ))
        return self._send_itWithHeader(_apiqueryurl, self._token, self._datasetid, self._datasetName)

    """
    Set the color of the button to some more custom value. Options are in StatusVsColor.StatusVsColor
    """
    def SetColor(self, StatusVsColor, burst=False):
        _apiqueryurl = urljoin(
            self._host,
            "api/DriverIn/SetColor/{0}/{1}/{2}/{3}".format
            (
                self._token, self._datasetName, StatusVsColor.value, self._datasetid
            ))
        return self._send_itWithHeader(_apiqueryurl, self._token, self._datasetid, self._datasetName)

    """
    Set the color of the button to some more custom value. Options are in StatusVsColor.StatusVsColor
    """
    def UnSetColor(self, StatusVsColor, burst=False):
        _apiqueryurl = urljoin(
            self._host,
            "api/DriverIn/UnSetColor/{0}/{1}/{2}".format
            (
                self._token, self._datasetName, self._datasetid
            ))
        return self._send_itWithHeader(_apiqueryurl, self._token, self._datasetid, self._datasetName)


    def _send_it(self, url, burst=False):
        if burst:
            jsondata = requesthelper_queue(url)
        else:
            jsondata = requesthelper(url)
        if(jsondata is not None):
            return jsondata
        else:
            return None

    def _send_itWithHeader(self, url, key, id, name):
        jsondata = requesthelperWithHeader(url, key, id, name, "DataSet")
        if(jsondata is not None):
            return jsondata
        else:
            return None

