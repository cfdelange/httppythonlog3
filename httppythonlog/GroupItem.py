"""
GroupItems holds the names and id of a dataset.
GroupItems belong to a group with a Group name
"""

class GroupItem(object):

    def __init__(self, groupitem):
        self.iD = groupitem[u'iD']
        self.dataSetName = groupitem[u'dataSetName']
        self.dataSetID = groupitem[u'dataSetID']
        self.dateCreated = groupitem[u'dateCreated']
        self.groupID = groupitem[u'groupID']