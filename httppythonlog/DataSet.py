from httppythonlog.DataSetMeta import DataSetMeta
from httppythonlog.DriverIn import DriverInput


class DataSet(object):

    def __init__(self, dataset):
        self.datasetID = dataset[u'datasetID']
        self.taskID = dataset[u'taskID']
        self.name = dataset[u'name']
        self.sixDigitCode = dataset[u'sixDigitCode']
        self.dateCreated = dataset[u'dateCreated']
        self.meta = DataSetMeta(dataset[u'dataSetMeta'])
        self.driverin = DriverInput(dataset[u'dvInput'])
