import json

class DriverInput(object):

    def __init__(self, driverinput):
        self.id = driverinput[u'id']
        self.jsondata = json.loads(driverinput['jsondata'])
