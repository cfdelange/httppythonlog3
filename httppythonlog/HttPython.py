from urllib.parse import urljoin
from httppythonlog.Helper import requesthelper, requesthelperWithHeader
from httppythonlog.DataSet import DataSet
from httppythonlog.Group import Group
from httppythonlog.Logger import LogStuff_Logger
from httppythonlog.FileToken_manager import filetoken_manager
from httppythonlog.StatusVscolor import StatusVsColor


class DataSet_User(object):

    def __init__(self, host, dataset):
        self.dataset = DataSet(dataset)
        self.logger = LogStuff_Logger(
            host,
            self.dataset
            )


class LogStuff_Task_User(object):

    def __init__(self, host, token, name):
        self._valid_host = True
        self._host = host
        self._atask_get_token = token
        try:
            self._token, self._taskID = self.exctract_token_id(token)
        except:
            return
        self._name = name
        self.list_datasets = []
        self.list_groups = []
        # This has to run on initialization
        self._iptoken = ''
        self.tokenmanager = None
        self._get_datasets()

    def exctract_token_id(self, token):
        splitToken = token.split("-")
        token = splitToken[1]
        idTask = splitToken[0]
        if token is None or idTask is None:
            return None, None
        if not(int(idTask) > 0 and len(token) > 0):
            return None, None
        return token, idTask

    def print_datasets(self):
        count = 0
        for var in self.list_datasets:
            count += 1
            print("Count: {}, Name: {}, Token: {}, dsid: {}, Pass: {}, CollideAll: {}, CollideSome: {}".format(
                count,
                var.dataset.name,
                var.dataset.sixDigitCode,
                var.dataset.datasetID,
                var.dataset.meta._pass,
                var.dataset.meta.collideAll,
                var.dataset.meta.collideSome
                ))

    def clear_dataset(self, dataset_name):
        for dsu in self.list_datasets:
            if(dsu.dataset.name == dataset_name):
                _apiqueryurl = urljoin(self._host, "api/DataSet/Clear/{0}".format(dsu.dataset.datasetID))
                break
        else:
            return -1
        r = requesthelperWithHeader(_apiqueryurl, self._token, self._taskID, self._name, "ATask")
        if(isinstance(r, int)):
            return r
        else:
            return -23455

    def delete_dataset(self, dataset_name):
        for dsu in self.list_datasets:
            if(dsu.dataset.name == dataset_name):
                _apiqueryurl = urljoin(self._host, "api/DataSet/Delete/{0}".format(dsu.dataset.datasetID))
                break
        else:
            return -1
        r = requesthelperWithHeader(_apiqueryurl, self._token, self._taskID, self._name, "ATask")
        if(isinstance(r, int)):
            if(r > 1):
                self.list_datasets.remove(dsu)
            return r
        else:
            return -234556

    def clear_group(self, group_name):
        for group in self.list_groups:
            if(group.groupName == group_name):
                _apiqueryurl = urljoin(self._host, "api/Group/Clear/{0}".format(group.groupID))
                break
        else:
            return -1
        r = requesthelperWithHeader(_apiqueryurl, self._token, self._taskID, self._name, "ATask")
        if(isinstance(r, int)):
            return r
        else:
            return -23457

    def delete_group(self, group_name):
        for group in self.list_groups:
            if (group.groupName == group_name):
                _apiqueryurl = urljoin(self._host, "api/Group/Delete/{0}".format(group.groupID))
                break
        else:
            return -1
        r = requesthelperWithHeader(_apiqueryurl, self._token, self._taskID, self._name, "ATask")
        if (isinstance(r, int)):
            return r
        else:
            return -23458

    def group(self, dataset_name, group_name):
        """
        Group datasets.
        Add the dataset name and the group_name to a groupitem
        It the dataset or group does not exist then it gets created.
        """
        dsu = self._check_else_create(dataset_name)
        group = self._check_group_else_create(group_name)
        if(isinstance(dsu, int) or isinstance(group, int)):
            return None
        if(dsu is not None and group is not None):
            _apiqueryurl = urljoin(self._host, "api/GroupItem/Create/{0}/{1}/{2}/{3}".format(
                                       dsu.dataset.datasetID, group.groupName,
                                       dsu.dataset.name, group.groupID
                                   ))
            jsondata = self._request(_apiqueryurl)
            if (jsondata is not None):
                return jsondata
            else:
                return None

    def setlimits_dataset(self, dataset_name, x_high_limit, y_high_limit, x_low_limit, y_low_limit, burst=False):
        dsu = self._check_else_create(dataset_name)
        if(dsu is not None):
            return dsu.logger.LogRectangle(int(x_low_limit), int(y_high_limit),
                                  abs(int(x_high_limit) - int(x_low_limit)),
                                  abs(int(y_high_limit) - int(y_low_limit)), burst)

    def xy(self, dataset_name, x, y, burst=False):
        dsu = self._check_else_create(dataset_name)
        if(dsu is not None):
            return dsu.logger.LogXY(int(x), int(y), burst)

    def text(self, dataset_name, x, y=0, burst=False):
        dsu = self._check_else_create(dataset_name)
        if(dsu is not None):
            return dsu.logger.LogText(x, int(y), burst)

    def y(self, dataset_name, y, burst=False):
        dsu = self._check_else_create(dataset_name)
        if(dsu is not None):
            return dsu.logger.LogY(int(y), burst)

    def dataset_pass(self, dataset_name, b_pass, burst=False):
        """
        Set pass or fail manually on dataset.
        """
        dsu = self._check_else_create(dataset_name)
        if(dsu is not None):
            return dsu.logger.DatasetPass(bool(b_pass), burst)

    def setStatusColor(self, dataset_name, StatusVsColor, burst=False):
        """
        Set the status so that it reflects in a color in the logging
        dashboard
        """
        dsu = self._check_else_create(dataset_name)
        if(dsu is not None):
            return dsu.logger.SetColor(StatusVsColor, burst)

    def unsetStatusColor(self, dataset_name, StatusVsColor, burst=False):
        """
        UnSets the status so that it reflects in a color in the logging
        dashboard
        """
        dsu = self._check_else_create(dataset_name)
        if(dsu is not None):
            return dsu.logger.UnSetColor(StatusVsColor, burst)

    def _find_by_name(self, dataset_name):
        for dsu in self.list_datasets:
            if(dsu.dataset.name == dataset_name):
                return dsu
        else:
            return None

    def _check_else_create(self, dataset_name):
        """
        Gets the datasetuser or create the dataset with the api
        if it does not exist
        """
        for dsu in self.list_datasets:
            if dsu.dataset.name == dataset_name:
                return dsu
        else:
            newdataset = self.create_dataset(dataset_name)
            if isinstance(newdataset, dict):
                if 'id' in newdataset.keys():
                    if newdataset['id'] < 1:
                        return None
            if isinstance(newdataset, int) is True:
                return newdataset
            if newdataset is None:
                return None
            elif int(newdataset[u'datasetID']) > 0:
                ds_user = DataSet_User(self._host, newdataset)
                self.list_datasets.append(ds_user)
                return ds_user
            else:
                return None

    def _check_group_else_create(self, group_name):
        """
        Get the group or create it with the API if it does not exist
        """
        for group in self.list_groups:
            if(group.groupName == group_name):
                return group
        else:
            newgroup = self.create_group(group_name)
            if isinstance(newgroup, dict):
                if 'id' in newgroup.keys():
                    if newgroup['id'] < 1:
                        return None
            if isinstance(newgroup, int) is True:
                return newgroup
            if newgroup is None:
                return None
            if isinstance(newgroup, int):
                return newgroup
            elif int(newgroup[u'groupID']) > 0:
                #ds_user = DataSet_User(self._host, newgroup)
                self.list_groups.append(newgroup)
                return newgroup
            else:
                return None

    def create_group(self, new_group_name):
        """
        Create a group. Added to task to group datasets
        belonging to that task
        """
        _apiqueryurl = urljoin(self._host,
                               "api/Group/CreatefromID/{0}/{1}/{2}/{3}".format(
                                   self._token, self._name,
                                   new_group_name, self._taskID))
        newGroup = requesthelperWithHeader(_apiqueryurl, self._token, self._taskID, self._name, "ATask" )
        if(isinstance(newGroup, dict)):
            if('id' in newGroup):
                return newGroup['id']
        if not isinstance(newGroup, int) and (newGroup is not None):
            if newGroup is None:
                return None
            elif int(newGroup[u'groupID']) > 0:
                groupObject = Group(newGroup)
                self.list_groups.append(groupObject)
                return groupObject
        else:
            if isinstance(newGroup, int):
                return newGroup
            else:
                return None

    def _request(self, url, timeout=2):
        return requesthelper(url, timeout)


class IP_token_Task_User(LogStuff_Task_User):

    def __init__(self, host_hog_mysql, token, name):
        super(IP_token_Task_User, self).__init__(host_hog_mysql, token, name)

    def _get_datasets(self):
        """
        Override inherited
        Log IP data from specific IP data, similar data, lots of ip's
        """
        self.tokenmanager = filetoken_manager()
        if(len(self._iptoken) < 6):
            filetoken = self.tokenmanager.read_from_file()
            if(len(filetoken) > 6):
                self._iptoken = filetoken
            if(filetoken == ''):
                self._iptoken = "apiget"
        _apiqueryurl = urljoin(self._host,"api/ATask/GetEagerPerIPtoken/{0}/{1}/{2}".format(
                                   self._atask_get_token,
                                   self._name,
                                   self._iptoken
                                   )
                               )
        #set timeout to 5 seconds
        jsondata = requesthelperWithHeader(_apiqueryurl, self._token, self._taskID, self._name, "ATask", 5, 3)
        if('id' in jsondata):
            if(jsondata[u'id'] < 1):
                return None
        if(jsondata is not None):
            self._taskID = jsondata[u'taskID']
            if(self._iptoken != jsondata[u'ip']):
                self._iptoken = jsondata[u'ip']
                if(self._iptoken is None):
                    return
                else:
                    # save the ip token, to identify this node
                    self.tokenmanager.write_to_file(self._iptoken)
            self.list_datasets[:] = []
            self.list_groups[:] = []
            for dataset in jsondata[u'dataSets']:
                ds = DataSet_User(self._host, dataset)
                self.list_datasets.append(ds)
            for group in jsondata[u'groups']:
                self.list_groups.append(Group(group))
            self._valid_host = True
        else:
            self._valid_host = False

    def create_dataset(self, new_dataset_name):
        """
        Overload original for now, need when
        create new DataSet, get ATask by id in API
        """
        _apiqueryurl = urljoin(self._host,
                               "api/DataSet/CreatefromID/{0}/{1}/{2}/{3}".format(
                                   self._token, self._name,
                                   new_dataset_name, self._taskID))
        r = requesthelperWithHeader(_apiqueryurl, self._token, self._taskID, self._name, "ATask")
        if not isinstance(r, int) and (r is not None):
            return r
        else:
            # HttpPythonLog error
            return None

if __name__ == '__main__':
    '''
    A Task can hold multiple datasets.
    A Dataset can hold multiple types of telemetry.
    Rectangles, Text, XY and Y vs datetime data
    Put your ip address and the port number in the function below.
    When you create a task in logstuff, give it a name, a 6 digit code will be created for you
    '''
    logstuff = IP_token_Task_User("http://<your_ip>:9673", "SU6NOX", "tasky")

    '''
    Type any name, it will be added to the Task in logstuff as you log it.
    Set limits for you dataset, if you want for you xy data.
    setlimits("datset_name", x_high_limit, y_high_limit, x_low_limit, y_low_limit)
    '''
    v = logstuff.setlimits_dataset("dataset_name1000", 50, 0, 10, 10)
    '''
    log some xy values to it and check on logstuff if the pass or fail
    '''
    logstuff.xy("dataset_name1000", 40, 5)

    '''
    To log text to the same dataset
    '''
    logstuff.text("dataaset_name1000", "Text in my dataset")

    '''
    To log text to a new, different dataset, the dataset will be created if it does not exist
    '''
    logstuff.text("dataaset_name1001", "Text in my dataset")

    '''
    To log y vs datatime values.
    '''
    logstuff.y("dataset_name1001", 7)






