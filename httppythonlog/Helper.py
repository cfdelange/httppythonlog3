import requests
import threading
import queue
import time
import datetime

requests.adapters.DEFAULT_RETRIES = 0

_q = queue.Queue()


def worker():
    while not _q.empty():
        url = _q.get()
        time.sleep(5)
        requesthelper(url)
        _q.task_done()


class the_thread:
    def __init__(self):
        self.the_thread = None

    def get_thread(self):
        return the_thread


my_single_thread = the_thread()


def requesthelper_queue(url):
    _q.put(url)
    if my_single_thread.the_thread is None or not my_single_thread.the_thread.is_alive():
        # initialize the worker thread and start it.
        my_single_thread.the_thread = threading.Thread(target=worker, daemon=True, name="{}".format(datetime.datetime.now()))
        my_single_thread.the_thread.start()
    return {"burst": True}


def requesthelper(url, timeout=2, rate_limit=2):
    time.sleep(rate_limit)
    try:
        r = requests.get(url, timeout=timeout)
    except requests.exceptions.ReadTimeout as e:
        return {'id': -1, 'message': e.args}
    except Exception as e:
        return {'id': -2, 'message': e.args}
    if r.status_code in (200, 401):
        return r.json()
    else:
        return {'id': -4, 'message':r.status_code}


def requesthelperWithHeader(url, key, id, name, objtype_string, timeout=2, rate_limit=2):
    time.sleep(rate_limit)
    headers_dict = {"key": key, "id": str(id), "name": name, "auth": objtype_string}
    try:
        r = requests.get(url, timeout=timeout, headers = headers_dict)
    except requests.exceptions.ReadTimeout as e:
        return {'id': -1, 'message': e.args}
    except Exception as e:
        return {'id': -2, 'message': e.args}
    if r.status_code in (200, 401):
        return r.json()
    else:
        return {'id': -4, 'message':r.status_code}

def requesthelperpostHeader(url, key, id, name, objtype_string, data, json, timeout=2, rate_limit=2):
    time.sleep(rate_limit)
    headers_dict = {"key": key, "id": str(id), "name": name, "auth": objtype_string}
    try:
        r = requests.post(url, data, json, timeout=timeout, headers=headers_dict)
    except requests.exceptions.ReadTimeout as e:
        return {'id': -1, 'message': e.args}
    except Exception as e:
        return {'id': -2, 'message': e.args}
    if r.status_code in (200, 401):
        return r.json()
    else:
        return {'id': -4, 'message':r.status_code}


def doggy():
    pass


def requesthelperpost(url, data, json, timeout=2):
    time.sleep(2)
    try:
        r = requests.post(url, data, json,  timeout=timeout)
    except requests.exceptions.ReadTimeout as e:
        return {'id': -1, 'message': e.args}
    except Exception as e:
        return {'id': -2, 'message': e.args}
    if r.status_code in (200, 401):
        return r.json()
    else:
        return {'id': -4, 'message':r.status_code}


def requesthelper_delete(url, timout=2):
    try:
        r = requests.delete(url,  timeout=timout)
    except requests.exceptions.ReadTimeout as e:
        return {'id': -1, 'message': e.args}
    except Exception as e:
        return {'id': -2, 'message': e.args}
    if r.status_code in (200, 401):
        return r.json()
    else:
        return {'id': -4, 'message':r.status_code}


if __name__ == '__main__':
    v = requesthelper(r'http://192.***.1.**:9673/api/ATask/GetEagerPerIPtoken/TaskToken/TaskName/TaskIPtoken')
    print(type(v))

