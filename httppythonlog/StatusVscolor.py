from enum import Enum

class StatusVsColor(Enum):
    Fault = "Fault"
    NoData = "NoData"
    Error = "Error"
    Bad = "Bad"
    Good = "Good"
    Ignore = "Ignore"
