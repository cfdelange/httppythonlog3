

class DataSetMeta(object):

    def __init__(self, datasetmeta):

        self.id = datasetmeta[u'id']
        self.countY = datasetmeta[u'countY']
        self.countCoord = datasetmeta[u'countCoord']
        self.countRect = datasetmeta[u'countRect']
        self.limitY = datasetmeta[u'limitY']
        self.limitCoord = datasetmeta[u'limitCoord']
        self.limitRect = datasetmeta[u'limitRect']
        self.checkCollision = datasetmeta[u'checkCollision']
        self.collideAll = datasetmeta[u'collideAll']
        self.collideSome = datasetmeta[u'collideSome']
        self._pass = datasetmeta[u'pass']
