import setuptools

setuptools.setup(
    name='httppythonlog',
    version='0.1',
    #packages=['httppythonlog',],
    packages=setuptools.find_packages(),
    author='Cornel de Lange',
    author_email='corneldelange@hotmail.com',
    maintainer='Cornel de Lange',
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.md').read(),
    python_requires='>=3.6',
)